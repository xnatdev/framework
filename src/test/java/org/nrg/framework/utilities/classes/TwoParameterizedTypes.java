package org.nrg.framework.utilities.classes;

public interface TwoParameterizedTypes<K, V>  {
    V get(K key);
}
